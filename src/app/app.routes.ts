
import { RouterModule, Routes } from "@angular/router";
import { AboutComponent } from "./components/about/about.component";
import { BuscadorComponent } from "./components/buscador/buscador.component";
import { HeroeComponent } from "./components/heroe/heroe.component";
import { HeroesComponent } from "./components/heroes/heroes.component";
import { HomeComponent } from "./components/home/home.component";

const APP_ROUTES: Routes = [ //de tipo rutas
  {path: 'home', component: HomeComponent },  //se le da a 'home' el componente HOME
  {path: 'about', component: AboutComponent},
  {path: 'heroes', component: HeroesComponent},
  {path: 'buscar/:termino', component: BuscadorComponent}, //añadimos un nuevo componente
  
  {path: 'heroe/:id', component: HeroeComponent}, //lo adicionamos despues de que creamos otro componente desde cmd
  {path: '**', pathMatch: 'full', redirectTo: 'home'} //est es un redireccionamiento
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);