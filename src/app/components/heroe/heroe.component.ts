import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from 'src/app/servicios/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

  heroe: any = {}; //heroe de tipo any tiene un json vacio

  //inyeccion en el constructor
  constructor(private activatedRoute: ActivatedRoute, 
              private _heroesService: HeroesService) { //se inyecto el servicion en _heroesService
    this.activatedRoute.params.subscribe(params=>{
      console.log(params); //imprime un json
      console.log(params['id']); //imprime el elemento del json como cadena
      this.heroe = this._heroesService.getHeroe(params['id']); //al heroe: any que tiene un json vacio le igualamos la clase HeroesService que tiene el json con la informacion
      console.log(this.heroe);
      
    });
  }

  ngOnInit(): void {
  }

}
