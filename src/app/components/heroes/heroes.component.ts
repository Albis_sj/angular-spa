import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeroesService, Heroe } from '../../servicios/heroes.service'; //se añadio ,Heroes porque es la interface

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit { //esta implementando, es una interfaz

  heroes:any[] = []; //aqui se dice que heroes es un array vacion de tipo any[]

  //el servicio funciona inyectando en el constructor 
  constructor( private _heroesService : HeroesService, 
    private router : Router) {
    console.log("constructor");
    
  } 

  ngOnInit(): void {
    console.log("ngOnInit");
    //despues de hacer heroes: any[]
    this.heroes = this._heroesService.getHeroes() //de la calse se obtiene el getter
    console.log(this.heroes); //imprime un array que copiamos en heroes.service
    
  }

  verHeroe(idx: number): void{
    console.log(idx);
    this.router.navigate(['/heroe', idx]) //esto te redirecciona a otra pagina con el enlace /heroe/id
    
    
  }

}
