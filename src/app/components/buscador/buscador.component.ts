import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroesService } from 'src/app/servicios/heroes.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

  heroes: any[]= []; //puede ser any, o de tipo interface Heroe de service
  termino!: string; //se le coloca la negacion para que no moleste

  constructor(private activatedRoute: ActivatedRoute, 
              private router: Router, 
              private heroesService: HeroesService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => { //subscribe obtiene una variable
      console.log(params['termino']);
      this.termino = params['termino']; //se lo guarda en una variables lo que hace log, y esa variable 
      
      this.heroes = this.heroesService.buscarHeroes(this.termino);

      console.log(this.heroes);
      
    })
  }

  verHeroe (idx:number){
    console.log(idx);
    this.router.navigate(['/heroe', idx]);
    
  }

}
